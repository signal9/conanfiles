from conans import ConanFile, tools


class RapidjsonConan(ConanFile):
    name = "rapidjson"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/miloyip/rapidjson"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "A fast JSON parser/generator for C++ with both SAX/DOM style API."
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/miloyip/rapidjson/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy(
            "*",
            dst="rapidjson",
            src="rapidjson-" + str(self.options.revision) + "/include/rapidjson",
        )

    def package_info(self):
        self.cpp_info.includedirs = [""]

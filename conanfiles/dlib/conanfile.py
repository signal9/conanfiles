from conans import ConanFile, CMake, tools
from os.path import exists


class DlibConan(ConanFile):
    name = "dlib"
    version = "<VERSION>"
    license = "BSL"
    url = "https://github.com/davisking/dlib"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "A toolkit for making real world machine learning and data analysis applications in C++"

    def build(self):
        tools.get(
            "https://github.com/davisking/dlib/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={"DLIB_NO_GUI_SUPPORT": "ON"},
            source_dir="dlib-" + str(self.options.revision),
        )
        cmake.install()

    def package(self):
        self.copy(
            "*.h",
            dst="include/dlib",
            src="dlib-" + str(self.options.revision) + "/dlib",
        )

    def package_info(self):
        self.cpp_info.libs = ["dlib"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

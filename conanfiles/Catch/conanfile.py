from conans import ConanFile, tools


class CatchConan(ConanFile):
    name = "Catch"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/catchorg/Catch2"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "A modern, C++-native, header-only, test framework for unit-tests, TDD and BDD - using C++11, C++14, C++17 and later"
    license = "MIT"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/catchorg/Catch2/{}/single_include/catch2/catch.hpp".format(
                str(self.options.revision)
            ),
            "catch.hpp",
        )

    def package(self):
        self.copy("*.hpp")

    def package_info(self):
        self.cpp_info.includedirs = [""]

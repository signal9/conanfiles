from conans import ConanFile, tools


class TypeSafeConan(ConanFile):
    name = "type_safe"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/foonathan/type_safe"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    requires = "debug_assert/[>=1.3.3]@signal9/stable"
    description = "Zero overhead utilities for preventing bugs at compile time"
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/foonathan/type_safe/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*.hpp", src="type_safe-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]

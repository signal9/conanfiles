#include "function_ref.hpp"

void foo (tl::function_ref<int(int)> func) { }

int main() {
    foo([](int x){return x;});
}

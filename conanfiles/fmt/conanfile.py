from conans import ConanFile, CMake, tools
from os.path import exists


class FmtConan(ConanFile):
    name = "fmt"
    version = "<VERSION>"
    license = "BSD-2-Clause"
    url = "https://github.com/fmtlib/fmt"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "fmt is an open-source formatting library for C++. It can be used as a safe alternative to printf or as a fast alternative to IOStreams."

    def build(self):
        tools.get(
            "https://github.com/fmtlib/fmt/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={"FMT_DOC": "OFF", "FMT_TEST": "OFF"},
            source_dir="fmt-" + str(self.options.revision),
        )
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["fmt"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))


# from os.path import exists
# self.cpp_info.libdirs = list(filter(exists,('lib32','lib64','lib')))

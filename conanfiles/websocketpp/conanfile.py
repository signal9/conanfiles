from conans import ConanFile, tools


class WebsocketppConan(ConanFile):
    name = "websocketpp"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/zaphoyd/websocketpp"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "C++ websocket client/server library"
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/zaphoyd/websocketpp/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        tools.replace_in_file(
            "websocketpp-"
            + str(self.options.revision)
            + "/websocketpp/common/memory.hpp",
            "using std::auto_ptr;",
            "",
        )

    def package(self):
        self.copy(
            "*.hpp",
            dst="websocketpp",
            src="websocketpp-" + str(self.options.revision) + "/websocketpp",
        )

    def package_info(self):
        self.cpp_info.includedirs = [""]

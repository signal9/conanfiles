from conans import ConanFile, CMake, tools
from os.path import exists


class NanodbcConan(ConanFile):
    name = "nanodbc"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/lexicalunit/nanodbc"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "A small C++ wrapper for the native C ODBC API."

    def build(self):
        tools.get(
            "https://github.com/lexicalunit/nanodbc/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={"NANODBC_TEST": "OFF", "NANODBC_EXAMPLES": "OFF"},
            source_dir="nanodbc-" + str(self.options.revision),
        )
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["nanodbc"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

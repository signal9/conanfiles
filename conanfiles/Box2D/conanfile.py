from conans import ConanFile, tools, Meson
from os import chdir
from pathlib import Path

MESON_BUILD = """\
project('box2d', 'cpp')
library(meson.project_name(), [{}],
include_directories : include_directories('.'))
"""


class Box2DConan(ConanFile):
    name = "Box2D"
    version = "<VERSION>"
    license = "Zlib"
    url = "https://github.com/erincatto/Box2D"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "Box2D is a 2D physics engine for games"

    def build(self):
        tools.get(
            "https://github.com/erincatto/Box2D/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        foldr = "Box2D-" + str(self.options.revision) + "/Box2D"
        chdir(foldr)
        tools.save(
            "meson.build",
            MESON_BUILD.format(
                ",".join(map("'{}'".format, map(str, Path(".").glob("Box2D/**/*.cpp"))))
            ),
        )
        meson = Meson(self)
        meson.configure(source_folder=foldr, build_folder="build")
        meson.build()

    def package(self):
        self.copy(
            "*.h",
            dst="include/Box2D",
            src="Box2D-" + str(self.options.revision) + "/Box2D/Box2D",
        )
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["box2d"]

from conans import ConanFile, CMake, tools
from os.path import exists


class Re2Conan(ConanFile):
    name = "re2"
    version = "<VERSION>"
    license = "BSD-3-Clause"
    url = "https://github.com/google/re2"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "RE2 is a fast, safe, thread-friendly alternative to backtracking regular expression engines like those used in PCRE, Perl, and Python. It is a C++ library."

    def build(self):
        tools.get(
            "https://github.com/google/re2/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(
            defs={"BUILD_TESTING": "OFF"},
            source_dir="re2-" + str(self.options.revision),
        )
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["re2"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

from conans import ConanFile, tools


class TextcsvConan(ConanFile):
    name = "text-csv"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/roman-kashitsyn/text-csv"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "C++ CSV read/write library"
    license = "Boost Software License"

    def build(self):
        tools.get(
            "https://github.com/roman-kashitsyn/text-csv/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*.hpp", src="text-csv-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]

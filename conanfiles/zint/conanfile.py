from conans import ConanFile, CMake, tools
from os.path import exists


class ZintConan(ConanFile):
    name = "zint"
    version = "<VERSION>"
    license = "GPLv3"
    url = "https://github.com/zint/zint"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "Zint is a suite of programs to allow easy encoding of data in any of the wide range of public domain barcode standards and to allow integration of this capability into your own programs."

    def build(self):
        tools.get(
            "https://github.com/zint/zint/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(source_dir="zint-" + str(self.options.revision))
        cmake.install()

    def package(self):
        self.copy(
            "*.h", dst="include", src="zint-" + str(self.options.revision) + "/backend"
        )
        self.copy("*.so*", dst="lib", src="backend", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["zint"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

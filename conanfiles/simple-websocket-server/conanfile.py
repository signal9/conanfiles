from conans import ConanFile, tools


class SimpleWebsocketServerConan(ConanFile):
    name = "simple-websocket-server"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/eidheim/Simple-WebSocket-Server"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "A very simple, fast, multithreaded, platform independent WebSocket (WS) and WebSocket Secure (WSS) server and client library"
    license = "MIT"

    def build(self):
        tools.get(
            "https://github.com/eidheim/Simple-WebSocket-Server/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*.hpp", src="Simple-WebSocket-Server-" + str(self.options.revision))

    def package_info(self):
        self.cpp_info.includedirs = [""]

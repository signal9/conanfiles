from conans import ConanFile, tools


class BeastConan(ConanFile):
    name = "Beast"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/boostorg/beast"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "HTTP and WebSocket built on Boost.Asio in C++11"
    license = "BSL-1.0"

    def build(self):
        tools.get(
            "https://github.com/boostorg/beast/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*", src="beast-" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]

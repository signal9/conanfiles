from conans import ConanFile, tools

# Example usage:
#
# include(${CONAN_COTIRE_ROOT}/cotire.cmake)
#


class CotireConan(ConanFile):
    name = "cotire"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/sakra/cotire"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "Cotire (compile time reducer) is a CMake module that speeds up the build process of CMake based build systems by fully automating techniques as precompiled header usage and single compilation unit builds for C and C++."
    license = "MIT"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/sakra/cotire/master/CMake/cotire.cmake",
            "cotire.cmake",
        )

    def package(self):
        self.copy("*.cmake")

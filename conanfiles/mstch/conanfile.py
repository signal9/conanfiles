from conans import ConanFile, CMake, tools
from os.path import exists


class MstchConan(ConanFile):
    name = "mstch"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/no1msd/mstch"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = (
        "mstch is a complete implementation of {{mustache}} templates using modern C++."
    )

    def build(self):
        tools.get(
            "https://github.com/no1msd/mstch/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(source_dir="mstch-" + str(self.options.revision))
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["mstch"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

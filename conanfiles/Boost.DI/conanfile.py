from conans import ConanFile, tools


class BoostdiConan(ConanFile):
    name = "Boost.DI"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/boost-experimental/di"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = (
        "Your C++14 header only Dependency Injection library with no dependencies."
    )
    build_policy = "missing"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/boost-experimental/di/cpp14/include/boost/di.hpp",
            "di.hpp",
        )

    def package(self):
        self.copy("*.hpp", dst="boost")

    def package_info(self):
        self.cpp_info.includedirs = [""]

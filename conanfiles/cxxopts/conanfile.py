from conans import ConanFile, tools


class CxxoptsConan(ConanFile):
    name = "cxxopts"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/jarro2783/cxxopts"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = "This is a lightweight C++ option parser library, supporting the standard GNU style syntax for options."
    license = "MIT"

    def source(self):
        tools.download(
            "https://raw.githubusercontent.com/jarro2783/cxxopts/master/include/cxxopts.hpp",
            "cxxopts.hpp",
        )

    def package(self):
        self.copy("*.hpp")

    def package_info(self):
        self.cpp_info.includedirs = [""]

from conans import ConanFile, tools


class BoostSIMDConan(ConanFile):
    name = "boost.simd"
    version = "<VERSION>"
    build_policy = "missing"
    url = "https://github.com/NumScale/boost.simd"
    options = {"revision": "ANY"}
    default_options = "revision=<REV>"
    description = (
        "Portable SIMD computation library - To be proposed as a Boost library"
    )
    license = "Boost Software License"

    def build(self):
        tools.get(
            "https://github.com/NumScale/boost.simd/archive/"
            + str(self.options.revision)
            + ".zip"
        )

    def package(self):
        self.copy("*", src="boost.simd" + str(self.options.revision) + "/include")

    def package_info(self):
        self.cpp_info.includedirs = [""]

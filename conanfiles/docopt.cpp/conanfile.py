from conans import ConanFile, CMake, tools
from os.path import exists
from os import remove


class DocoptCppConan(ConanFile):
    name = "docopt.cpp"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/docopt/docopt.cpp"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "docopt creates beautiful command-line interfaces."

    def build(self):
        tools.get(
            "https://github.com/docopt/docopt.cpp/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        cmake = CMake(self)
        cmake.configure(source_dir="docopt.cpp-" + str(self.options.revision))
        cmake.install()
        tools.save(
            self.package_folder + "/include/module.modulemap",
            """\
module docopt [system] {
	umbrella "docopt"
	export *
}
""",
        )
        remove(self.package_folder + "/include/docopt/docopt_private.h")
        remove(self.package_folder + "/include/docopt/docopt_util.h")

    def package_info(self):
        self.cpp_info.libs = ["docopt"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

from conans import ConanFile, CMake, tools
from os.path import exists
from os import replace


class BgfxConan(ConanFile):
    name = "bgfx"
    version = "<VERSION>"
    license = "BSD-2-Clause"
    url = "https://github.com/bkaradzic/bgfx"
    options = {"revision": "ANY", "shared": [True, False], "build_tools": [True, False]}
    default_options = "revision=<REV>", "shared=True", "build_tools=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = 'Cross-platform, graphics API agnostic, "Bring Your Own Engine/Framework" style rendering library.'

    def config_options(self):
        if self.settings.compiler == "emscripten":
            del self.options.build_tools

    def build(self):
        tools.get("https://github.com/JoshuaBrookover/bgfx.cmake/archive/master.zip")
        tools.get("https://github.com/bkaradzic/bimg/archive/master.zip")
        tools.get("https://github.com/bkaradzic/bgfx/archive/master.zip")
        tools.get("https://github.com/bkaradzic/bx/archive/master.zip")
        replace("bx-master", "bgfx.cmake-master/bx")
        replace("bimg-master", "bgfx.cmake-master/bimg")
        replace("bgfx-master", "bgfx.cmake-master/bgfx")

        cmake = CMake(self)
        if "build_tools" in self.options and self.options.build_tools:
            use_tools = "ON"
        else:
            use_tools = "OFF"
        cmake.configure(
            defs={
                "BGFX_INSTALL_EXAMPLES": "OFF",
                "BGFX_BUILD_EXAMPLES": "OFF",
                "BGFX_BUILD_TOOLS": use_tools,
            },
            source_dir="bgfx.cmake-master",
        )
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["bgfx", "bimg", "bx"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

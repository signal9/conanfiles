from conans import ConanFile, AutoToolsBuildEnvironment, tools
from os import chdir
from os.path import exists


class EnetConan(ConanFile):
    name = "enet"
    version = "<VERSION>"
    license = "MIT"
    url = "https://github.com/lsalzman/enet"
    options = {"revision": "ANY", "shared": [True, False]}
    default_options = "revision=<REV>", "shared=True"
    settings = "os", "compiler", "build_type", "arch"
    build_policy = "missing"
    description = "ENet reliable UDP networking library"

    def build(self):
        tools.get(
            "https://github.com/lsalzman/enet/archive/"
            + str(self.options.revision)
            + ".zip"
        )
        chdir("enet-" + str(self.options.revision))
        env_build = AutoToolsBuildEnvironment(self)
        with tools.environment_append(env_build.vars):
            self.run("autoreconf -vfi")
            self.run("./configure --prefix={}".format(self.package_folder))
            self.run("make")
            self.run("make install")

    def package_info(self):
        self.cpp_info.libs = ["enet"]
        self.cpp_info.libdirs = list(filter(exists, ("lib32", "lib64", "lib")))

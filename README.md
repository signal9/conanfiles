# signal9's Conan Repository

To add the remote:

```sh
conan remote add signal9 https://signal9.jfrog.io/artifactory/api/conan/conan
```

List of available packages:

https://signal9.jfrog.io/ui/packages?packageType=conan&type=packages

All packages have names in the form of `package_name/version@signal9/stable`.
